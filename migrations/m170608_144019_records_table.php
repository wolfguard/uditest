<?php

use yii\db\Migration;

class m170608_144019_records_table extends Migration
{
    public function up()
    {
        $table = '{{%records}}';
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'value' => $this->integer()->null(),
        ]);

        $data = [];
        for ($i = 1; $i <= 100 * 100; $i++) {
            $data[] = [$i];
        }
        Yii::$app->db->createCommand()->batchInsert($table, ['value'], $data)->execute();
    }

    public function down()
    {
        $this->dropTable('{{%records}}');
    }
}
