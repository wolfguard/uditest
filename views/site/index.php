<?php

/* @var $this yii\web\View */
/* @var $records array */

$this->title = 'My Yii Application';
$square = 100;
?>
<div class="site-index">

    <h1>This is uditest app!</h1>

    <div class="body-content">
        <p><button class="btn btn-success udi-save">Save</button></p>
        <div class="udi-table">
            <?php $i = 0; ?>
            <?php for ($r = 1; $r <= $square; $r++): ?>
                <div class="udi-row">
                    <?php for ($c = 1; $c <= $square; $c++): ?>
                        <?php $default = $records[$i]['value'] == $records[$i]['id']; ?>
                        <div class="udi-cell <?php echo $default ? '' : 'not-default';?>"
                             data-value="<?php echo $records[$i]['value']; ?>"
                             data-id="<?php echo $records[$i]['id']; ?>">&nbsp;</div>
                        <?php $i++; ?>
                    <?php endfor; ?>
                </div>
            <?php endfor; ?>
        </div>
        <div class="udi-tooltip"></div>
        <div class="udi-form"><input type="text" name="val"><button class="btn btn-default btn-xs udi-sub">OK</button></div>
    </div>
</div>
