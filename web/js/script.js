jQuery(document).ready(function($){
    window.tooltip = $('.udi-tooltip');
    window.form =  $('.udi-form');
    window.formInput = window.form.find('input');

    function hideTooltip(){
        window.tooltip.css({left: '-10000px'});
    }
    function hideForm(){
        window.form.css({left: '-10000px'});
    }

    $(document).on('mouseover', '.udi-cell', function(){
        var el = $(this);
        var position = el.position();
        window.tooltip.text(el.data('value'));
        window.tooltip.css({left: position.left+el.width(), top: position.top});
    });

    $(document).on('click', '.udi-cell', function(e){
        e.stopPropagation();
        var el = $(this);
        var position = el.position();
        window.formInput.val(el.data('value'));
        window.formInput.data('id', el.data('id'));
        window.form.css({left: position.left+el.width(), top: position.top});
        window.formInput.focus();

        hideTooltip();
    });

    window.formInput.keypress(function (e) {
        if (e.which == 13) {
            changeValue();
            return false;
        }
    });

    $('.udi-sub').on('click', function(e){
        e.stopPropagation();
        changeValue();
    });

    $('.udi-save').on('click', function(){
        hideForm();
        hideTooltip();

        var data = [];
        $('.udi-cell.changed').each(function(){
            var el = $(this);
            data.push({id: el.data('id'), value: el.data('value')});
            el.removeClass('changed');
        });
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: {data: JSON.stringify(data)}
        });
    });

    function changeValue(){
        var val = window.formInput.val();
        var id = window.formInput.data('id');
        var container = $('.udi-cell[data-id="'+id+'"]');
        if(container.data('value') != val){
            container.data('value', val);
            container.addClass('changed');
            if(id == val){
                container.removeClass('not-default');
            }else{
                container.addClass('not-default');
            }
        }
        hideForm();
    }
});